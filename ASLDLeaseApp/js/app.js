﻿var parcelmap;
var parcelIdList = [];

var geocoderZoomScale = 14;

var DefaultExtent = {};
DefaultExtent.xmin = -12461474;
DefaultExtent.ymin = 3949527;
DefaultExtent.xmax = -12457708;
DefaultExtent.ymax = 3951465;

var DefaultAddress = "1616 W Adams St, Phoenix, AZ 85007";

require([
  "esri/map", "esri/InfoTemplate", "esri/layers/FeatureLayer", "esri/tasks/query", "esri/dijit/Geocoder", "esri/graphic",
   "esri/renderers/SimpleRenderer",
  "esri/symbols/PictureMarkerSymbol",
   "dojo/dom",
  "dojo/on",
   "dojo/_base/event",
  "dojo/parser", "dojo/domReady!"
], function (
  Map, InfoTemplate, FeatureLayer, Query, Geocoder, Graphic, SimpleRenderer, PictureMarkerSymbol, dom, on, event,
  parser
) {
    parser.parse();
    parcelmap = new Map("mapDiv", {
        basemap: "streets",
        center: [-111.9385, 33.4204],
        zoom: 13
    });

    parcelmap.on("load", initOperationalLayer);

    var geocoder = new Geocoder({
        value: DefaultAddress,
        maxLocations: 3,
        autoComplete: true,
        arcgisGeocoder: { sourceCountry: "US" },
        map: parcelmap
    }, "geosearch");
    geocoder.startup();
    geocoder.on("select", geocodeSelect);
    geocoder.on("findResults", geocodeResults);
    geocoder.on("clear", clearFindGraphics);

    // Geosearch functions
    on(dom.byId("btnGeosearch"), "click", geosearch);

    function geosearch() {
        var def = geocoder.find();
        def.then(function (res) {
            geocodeResults(res);
        });
    }

    function geocodeSelect(item) {
        var g = (item.graphic ? item.graphic : item.result.feature);
        g.setSymbol(sym);
        addPlaceGraphic(item.result, g.symbol);
    }

    function geocodeResults(places) {
        places = places.results;
        if (places.length > 0) {
            clearFindGraphics();
            var symbol = sym;
            // Create and add graphics with pop-ups
            for (var i = 0; i < places.length; i++) {
                addPlaceGraphic(places[i], symbol);
                break;
            }
            //zoomToPlaces(places);
        } else {
            alert("Sorry, address or place not found.");
        }
    }

    function addPlaceGraphic(item, symbol) {
        parcelmap.graphics.clear();
        var place = {};
        var attributes, infoTemplate, pt;
        pt = item.feature.geometry;
        place.address = item.name;
        place.score = item.feature.attributes.score;
        // Graphic components
        attributes = { Address: place.address };
        incidentGraphic = new Graphic(pt, symbol, attributes);
        // Add to map
        parcelmap.graphics.add(incidentGraphic);
        parcelmap.centerAndZoom(pt, geocoderZoomScale);
        //fire off fuctions to run spatial query for leases, etc.
        var tempTimeout = setTimeout(function () { doSomeStuff(place.address) }, 1900);

    }

    function clearFindGraphics() {
        parcelmap.graphics.clear();
    }

    function doSomeStuff(in_address) {
        alert('Address is ' + in_address);
    }
    function createPictureSymbol(url, xOffset, yOffset, xWidth, yHeight) {
        return new PictureMarkerSymbol(
        {
            "angle": 0,
            "xoffset": xOffset, "yoffset": yOffset, "type": "esriPMS",
            "url": url,
            "contentType": "image/png",
            "width": xWidth, "height": yHeight
        });
    }

    var sym = createPictureSymbol("assets/purple-pin.png", 0, 12, 13, 24);

    function initOperationalLayer() {
        var infoTemplate = new InfoTemplate("${PARCEL}", "${*}");
        var featureLayer = new FeatureLayer("http://gis.azland.gov/adaptor/rest/services/StateTrust/SurfaceParcels/MapServer/0", {
            mode: FeatureLayer.MODE_ONDEMAND,
            outFields: ["*"],
            infoTemplate: infoTemplate
        });

        parcelmap.addLayer(featureLayer);
        parcelmap.infoWindow.resize(300, 150);

        featureLayer.on("update-start", function (evt) {
            //vt.target.graphics[0].attributes.BOND_IND
            //featureLayer.graphics.clear();
        });

        featureLayer.on("update-end", function (evt) {
            //vt.target.graphics[0].attributes.BOND_IND
            var arrLeaseCount = evt.target.graphics.length;
            $(".featureCount").html(arrLeaseCount);

            parcelIdList = [];

            if (evt.target.graphics.length > 0) {
                $.each(evt.target.graphics, function( index, value ) {
                    parcelIdList.push(value.attributes["PARCEL"]);
                });
                console.log(parcelIdList);
            }
            //optionally maybe we can fire off the ajax call here and post our list of parcelid or trs info to the server

            /*
            $.ajax({
                type: "POST",
                url: "Handlers/GetLeaseInBounds.ashx",
                data: parcelIdList,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    build_table_bounds(response);
                },
                error: function (data, textStatus, errorThrown) {
                    alert(data.status + " | " + data.statusText + " | " + data.responseText + " |\n\r " + textStatus + " | " + errorThrown);
                }
            });
            */
        });
    }
});
