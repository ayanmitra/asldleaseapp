﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Index.aspx.vb" Inherits="ASLDLeaseApp.Index" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ASLD Lease Information</title>
    <link rel="stylesheet" href="http://js.arcgis.com/3.14/dijit/themes/claro/claro.css" /> 
    <link rel="stylesheet" href="http://js.arcgis.com/3.14/esri/css/esri.css" />
    <link href="css/app.css" rel="stylesheet" />
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://js.arcgis.com/3.14/"></script>
</head>
<body class="claro">
    <div id="mapDiv">
        <div class="featureCount"></div>
        <div class="locateADDRESS" style="text-align:center">
                                <div class="form-group">
                                  <div id="geosearch"></div>                                   
                                </div>
                                <button id="btnGeosearch" type="submit" class="btn btn-success locatebtn">Find Address on Map</button>
                            </div> 
    </div>
    <form id="form1" runat="server">
    </form>  
    
    <script src="js/app.js"></script>
</body>
</html>
